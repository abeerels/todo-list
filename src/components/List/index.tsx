import React from 'react'
import { List } from 'antd'
import ListItem from './ListItem'

interface Props {
    data: Array<TodoItem>;
    onDeleteItem: (id: String) => void;
    onEditItem: (item: TodoItem) => void;
}

export type TodoItem = {
    text: string;
    id: string;
}

const TodoList: React.FunctionComponent<Props> = (props) => {
    return <List
        dataSource={props.data}
        renderItem={(item: TodoItem) => <ListItem item={item} onDeleteItem={props.onDeleteItem} onEditItem={props.onEditItem}/>}
    />
}

export default TodoList