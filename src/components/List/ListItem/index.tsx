import React, { ChangeEvent, useEffect, useState } from 'react'
import { Checkbox, Input, List, Typography, Button } from 'antd'
import { TodoItem } from '..'
import { DeleteOutlined } from '@ant-design/icons';
import './style.css'

interface Props {
    item: TodoItem;
    onDeleteItem: (id: String) => void;
    onEditItem: (item: TodoItem) => void;
}

const EditButton: React.FunctionComponent<{
    onEditItem: () => void;
    toggleEditMode: (bool: Boolean) => void
}> = ({ onEditItem }) => <Button type="link" onClick={() => onEditItem()}>edit</Button>

const CheckButton: React.FunctionComponent<{
    toggleCheckBox: () => void
}> = (props) => <Checkbox onClick={props.toggleCheckBox} />

const DeleteButton: React.FunctionComponent<{
    onDeleteItem: () => void;
}> = ({ onDeleteItem }) => <DeleteOutlined color="red" onClick={onDeleteItem} />

const ListItem: React.FunctionComponent<Props> = (props) => {
    const [isInEditMode, toggleEditMode] = useState<Boolean>(false)
    const [isChecked, setIsChecked] = useState<boolean>(false)
    const [updatedTodoItem, updateTodoItem] = useState<TodoItem>(props.item)

    const onTodoItemChange = (e: ChangeEvent<HTMLInputElement>, item: TodoItem) => {
        updateTodoItem({
            ...item,
            text: e.target.value
        })
    }

    const onEditClick = () => {
        if (isInEditMode) {
            toggleEditMode(false)
            updatedTodoItem && props.onEditItem(updatedTodoItem)
        } else {
            toggleEditMode(true)
        }
    }

    return <List.Item
        key={props.item.id}
        actions={[
            <CheckButton toggleCheckBox={() => setIsChecked(!isChecked)} />,
            <EditButton onEditItem={onEditClick} toggleEditMode={toggleEditMode} />,
            <DeleteButton onDeleteItem={() => props.onDeleteItem(props.item.id)} />
        ]}>
        {isInEditMode ?
            <Input onChange={e => onTodoItemChange(e, props.item)} value={updatedTodoItem.text} /> : <Typography.Text className={isChecked ? "todo-list-item-checked" : "todo-list-item"} >{props.item.text}</Typography.Text>}

    </List.Item>
}

export default ListItem