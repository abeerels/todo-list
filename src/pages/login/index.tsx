import React, { useContext, useEffect } from 'react'
import { Layout, Button, message } from 'antd'
import { FacebookOutlined } from '@ant-design/icons';
import firebase from 'firebase';
import { Context } from '../../router';
import { useHistory } from 'react-router-dom';
const Content = Layout.Content


const Login: React.FunctionComponent<{}> = (props) => {

    const { state, dispatch } = useContext(Context)
    const history = useHistory()

    useEffect(() => {
        console.log(state?.user);
        
        if (state?.user) {
            history.push('/home')
        }
    }, [state, history])

    const handleFacebookLogin = () => {
        const provider = new firebase.auth.FacebookAuthProvider();
        firebase
            .auth()
            .signInWithPopup(provider)
            .then((result: firebase.auth.UserCredential) => {
                const user = result.user;
                dispatch({
                    type: "UPDATE_USER",
                    payload: {
                        email: user?.email,
                        name: user?.displayName
                    }
                })
            })
            .catch((error) => {
                const errorMessage = error.message;
                message.error(errorMessage)
            });
    }


    return <Content className="container">
        <Button type="primary" shape="round" icon={<FacebookOutlined />} size={"large"} onClick={handleFacebookLogin}>
            {/* Supposed to be moved to language file */}
          Login via Facebook
        </Button>
    </Content>
}

export default Login