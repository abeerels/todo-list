import React, { ChangeEvent, useContext, useState } from 'react'
import { Button, Input, Layout, Row, Typography, } from 'antd'
import { Context } from '../../router';
import { PlusOutlined } from '@ant-design/icons';
import "./style.css"
import TodoList, { TodoItem } from '../../components/List';
import data from './data.json'
import { generateId } from '../../utils/helpers';

const Content = Layout.Content

const Home: React.FunctionComponent<{}> = () => {
    const [todolist, setTodoList] = useState<TodoItem[]>(data)
    const [addNewTodoTtem, toggleAddNewTodoItem] = useState<Boolean>(false)
    const [todoItem, setTodoItem] = useState<TodoItem | undefined>()

    const { state, dispatch } = useContext(Context)

    const addTodoTtem = (item: TodoItem) => {
        const newList: TodoItem[] = [...todolist, item]
        setTodoList(newList)
        cancleAddNewTodoItem()
    }

    const onTodoItemInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setTodoItem({ text: e.target.value, id: generateId() })
    }

    const cancleAddNewTodoItem: () => void = () => {
        toggleAddNewTodoItem(false)
        setTodoItem(undefined)
    }

    const onEditItem = (item: TodoItem) => {
        const newList: TodoItem[] = todolist.map(todoItem => {
            if (todoItem.id === item.id) {
                return {
                    ...todoItem,
                    text: item.text,
                }
            }
            else return todoItem

        })
        setTodoList(newList)
    }

    const onDeleteItem = (id: String) => {
        const newList: TodoItem[] = todolist.filter(item => item.id !== id)
        setTodoList(newList)
    }

    const logout: () => void = () => {
        dispatch({
            type: "LOGOUT"
        })
    }

    return <Content className="container">
        <Row className="header">
            <Button type="primary" onClick={logout} >Logout</Button>
        </Row>
        <Typography.Paragraph>
            {`Hi, ${state?.user?.name}. Welcome to my todo list app. \nPlease feel free to nav around`}
        </Typography.Paragraph>
        <TodoList data={todolist} onDeleteItem={onDeleteItem} onEditItem={onEditItem} />
        {addNewTodoTtem &&
            <Row>
                <Input onChange={onTodoItemInputChange} onPressEnter={() => todoItem && addTodoTtem(todoItem)} autoFocus/>
                <Button type="link" onClick={() => todoItem && addTodoTtem(todoItem)}>Add</Button>
                <Button type="link" danger onClick={cancleAddNewTodoItem}>Cancel</Button>
            </Row>
        }
        <Row>
            <Button
                type="primary"
                shape="circle"
                icon={<PlusOutlined />}
                size={"large"}
                onClick={() => toggleAddNewTodoItem(true)}
            />
        </Row>
    </Content>
}

export default Home