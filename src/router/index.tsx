import React, { useEffect, createContext, useReducer, Dispatch } from 'react'
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom'
import Login from '../pages/login'
import 'antd/dist/antd.css';
import BaseLayout from '../layouts/BaseLayout';
import { initializeApp } from '../utils/config';
import Home from '../pages/home';
import firebase from 'firebase';

interface User {
    email: String | null;
    name: String | null;
}

type State = {
    user: User | null;
}

const initialState: State = {
    user: null

}

const reducer = (state: State = initialState, action: any) => {
    switch (action.type) {
        case "UPDATE_USER":
            return {
                ...state,
                user: action.payload
            }
        case "LOGOUT":
            return {
                ...state,
                user: null
            }
        default: return state;
    }
}

export const Context = createContext<{
    state: State;
    dispatch: Dispatch<any>;
}>({
    state: initialState,
    dispatch: () => null
});

const App: React.FunctionComponent<{}> = () => {
    const [state, dispatch] = useReducer(reducer, initialState)

    useEffect(() => {
        firebase.apps.length === 0 && initializeApp()
    }, [state.user])

    const value = { state, dispatch };
    const isLoggedIn = state?.user
    return <Context.Provider value={value}>
        <Router>
            <BaseLayout>
                <Switch>
                    <Route exact path={"/"}>
                        {isLoggedIn ? <Home /> : <Redirect to={{ pathname: "/login" }} />}
                    </Route>
                    <Route exact path={"/home"}>
                        {isLoggedIn ? <Home /> : <Redirect to={{ pathname: "/login" }} />}
                    </Route>
                    <Route path={"/login"}>
                        <Login />
                    </Route>
                </Switch>
            </BaseLayout>
        </Router>
    </Context.Provider>

}

export default App